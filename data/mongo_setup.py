# -*- coding: utf-8 -*-
__author__ = "Hadi Azami, abtinmo"

import os
import mongoengine


def is_docker():
    path = '/proc/self/cgroup'
    return (
        os.path.exists('/.dockerenv') or
        os.path.isfile(path) and any('docker' in line for line in open(path))
    )


def init():
    if is_docker():
        mongoengine.register_connection(host="mongo", alias="bot", name="storage")
    else:
        mongoengine.register_connection(alias="bot", name="storage")
