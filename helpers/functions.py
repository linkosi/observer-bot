# -*- coding: utf-8 -*-
__author__ = "Hadi Azami"

import time
import logging

from pyrogram import InlineKeyboardMarkup, InlineKeyboardButton
from pyrogram.errors import (
    MessageDeleteForbidden,
    UserIsBlocked,
    PeerIdInvalid,
    ChatAdminRequired,
    RightForbidden
    )

from helpers.cfg import Config
from data import chat


def discard(messages, timeout=Config.discard_timeout):
    """delete given messages after timeout"""
    if timeout == 0:
        return
    try:
        time.sleep(timeout)
        for message in messages:
            message.delete()
    except MessageDeleteForbidden:
        logging.log(logging.WARNING, "don't have permission to delete message")
        return


def discard_reply(message):
    """discard the user reply that triggred the command"""
    try:
        message.delete()
    except MessageDeleteForbidden:
        logging.log(logging.WARNING, "don't have permission to delete message")


def report(client, message, is_bot=False):
    """ Report to admins """

    log_channel = chat.get(message.chat.id).log_channel
    if log_channel == 0:
        return

    if is_bot:
        me = client.get_me()
        reporter_name = me.first_name
        reporter_id = me.id
        reported_message = message.message_id
    else:
        reporter_name = message.from_user.first_name
        reporter_id = message.from_user.id
        reported_message = message.reply_to_message.message_id

    response = Config.responses.get("report")
    response = response.replace("first_name", reporter_name)
    response = response.replace("link", "tg://user?id={}".format(reporter_id))
    response = response.format(reported_message)

    client.forward_messages(
        chat_id=log_channel,
        from_chat_id=message.chat.id,
        message_ids=[reported_message]
    )
    callback = "delete,{},{}".format(message.chat.id, reported_message)
    markup = InlineKeyboardMarkup([
        [InlineKeyboardButton(
            text="حذف پیام - {}".format(reported_message),
            callback_data=callback
        )]
    ])

    client.send_message(
        chat_id=log_channel,
        text=response,
        reply_markup=markup
    )


def private_message(client, chat_id, text):
    """ Send private message """
    try:
        client.send_message(
            chat_id=chat_id,
            text=text,
            parse_mode="markdown"
        )
    except (UserIsBlocked, PeerIdInvalid) as e:
        reason = (
            "Failed to send private message to {} \n"
            "Reason: {}"
        ).format(chat_id, e)
        logging.log(logging.ERROR, reason)


def admin(client, chat_id, user_id):
    """ will check to see if given user in given chat is admin or creator """
    member = client.get_chat_member(
        chat_id=chat_id,
        user_id=user_id
    )
    status = member.status
    return True if status == "administrator" or status == "creator" else False


def owner(client, chat_id, user_id):
    """ owner is either the chat creator or somone who can promote members """
    member = client.get_chat_member(
        chat_id=chat_id,
        user_id=user_id
    )
    if member.status == "creator" or member.permissions.can_promote_members:
        return True
    return False


def check_permissions(order):
    """ verifies that bot has permissions to do the given order """
    def wrapper(*args):
        client = args[0]
        query = args[1]

        if hasattr(query, "chat"):
            chat_id = query.chat.id
        else:
            chat_id = query.message.chat.id

        try:
            order(*args)
        except (
            ChatAdminRequired,
            RightForbidden,
            MessageDeleteForbidden
        ):
            client.send_message(
                chat_id=chat_id,
                text="من دسترسی لازم برای انجام این عملیات را ندارم."
            )
    return wrapper
