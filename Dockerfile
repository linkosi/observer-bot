# Observer-bot
FROM python:3.6-alpine

# init
ADD . /code
WORKDIR /code

# prep
ENV PYTHONUNBUFFERED=1

# setup
RUN pip3 install -U pip && pip install -U -r requirements.txt

CMD ["python", "main.py"]
