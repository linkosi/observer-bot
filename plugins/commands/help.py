# -*- coding: utf-8 -*-

# command: !help
# alias: /help
# region: group, private
# description: Showing this message

"""Generate help"""

__author__ = "Hadi Azami"

from os import listdir
from os.path import isfile, join, abspath, dirname
from tokenize import tokenize, COMMENT

from pyrogram import Client, Filters


def extract(comment):
    try:
        key, value = comment.split(":")
        key = key.replace("#", "").lower().strip()
        value = value.strip()
        return {key: value}
    except ValueError:
        return {}


def find_comments():
    path = dirname(abspath(__file__))
    files = [f for f in listdir(path) if isfile(join(path, f))]
    commands = list()

    for f in files:
        with open(join(path, f), 'rb') as reader:
            command = dict()
            for token in tokenize(reader.readline):
                if token.type == COMMENT and token.string != "code":
                    command.update(extract(token.string))
        if bool(command):
            commands.append(command)
    return commands


def misc_commands():
    path = dirname(abspath(__file__))
    path = path.replace("/plugins/commands", "/responses/autogen")

    def fmt(f):
        """ format file name into command """
        return "!"+f.replace(".md", "")

    return [fmt(f) for f in listdir(path) if isfile(join(path, f))
            and not f.startswith(".")]


def prettyprint():
    info = find_comments()
    result = "COMMAND (ALIAS) (REGION) -- DESCRIPTION\n\n"
    for data in info:
        result += "{}".format(data.get('command'))
        if data.get('alias'):
            result += " ({})".format(data.get('alias'))
        result += " ({})".format(data.get('region'))
        result += " -- {}\n\n".format(data.get('description'))

    result += "\nMiscellaneous Commands: \n"
    for cmd in misc_commands():
        result += cmd + " "

    return result


response = prettyprint()


@Client.on_message(
    Filters.command(commands=["help"], prefix=["!", "/"])
    & (Filters.private | Filters.group)
)
def help(client, message):

    reply_to = None
    if message.reply_to_message:
        reply_to = message.reply_to_message.message_id

    message.reply_text(
        text=response,
        reply_to_message_id=reply_to,
        quote=not bool(reply_to),
        parse_mode=None
    )
