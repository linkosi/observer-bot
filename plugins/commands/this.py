# -*- coding: utf-8 -*-

# command: !this
# region: group
# description: Returns the group id

__author__ = "Hadi Azami"

from pyrogram import Client, Filters

from helpers.cfg import Config
from helpers.filters import reply_to_bot


@Client.on_message(
    Filters.command(["this"], "!")
    & ~ Filters.create(reply_to_bot)
    & Filters.group
)
def this(clien, message):
    message.reply_text(
        Config.responses.get("this").format(message.chat.id)
    )
