# -*- coding: utf-8 -*-

# command: !link
# region: group
# description: invitation link

""" Create new link every 3 hours base on requests """

__author__ = "Mr"

from pyrogram import Client, Filters

from helpers.cfg import Config
from helpers.filters import reply_to_bot
from helpers.functions import check_permissions

from datetime import datetime

GROUP_LINKS = dict()


def get_group_link(chat_id):
    """ Get group link from GROUP_LINKS dictionary """
    return GROUP_LINKS.get(
        chat_id,
        'Link expired! Please request again.'
    ).get('link', None)


def link_exist(chat_id):
    """ Check if link of group is exist or not """
    return True if GROUP_LINKS.get(chat_id, None) is not None else False


def create_link(client, chat_id):
    """ Craete group link and save it to GROUP_LINKS dictionary """
    GROUP_LINKS[chat_id] = {
        'link': client.export_chat_invite_link(chat_id),
        'expire': datetime.now()
    }


def link_valid(chat_id):
    """ Check is link expire or not """
    expire = GROUP_LINKS.get(chat_id, None).get('expire', None)
    if expire is not None:
        diff = datetime.now() - expire
        return True if diff.total_seconds() < (3 * 3600) else False
    return False


def send_message(client, message):
    """ Send message to user """
    if message.reply_to_message is not None:
        reply_to = message.reply_to_message.message_id
    else:
        reply_to = message.message_id

    response = Config.responses.get('link').format(
        get_group_link(message.chat.id)
    )

    client.send_message(
        chat_id=message.chat.id,
        text=response,
        reply_to_message_id=reply_to
    )


@Client.on_message(
    Filters.command(commands=["link"], prefix="!")
    & ~ Filters.create(reply_to_bot)
    & Filters.group
)
@check_permissions
def link(client, message):
    """ Main function. Handle !link command. """
    chat_id = message.chat.id

    if not message.chat.username:
        if not link_exist(chat_id) or not link_valid(chat_id):
            create_link(client, chat_id)
        send_message(client, message)
