# -*- coding: utf-8 -*-

# command: !id
# region: private
# description: Showing your unique Telegram ID

"""echo chat id"""

__author__ = "Ali Shokoohi"

from pyrogram import Client, Filters

from helpers.cfg import Config


@Client.on_message(
    Filters.private
    & Filters.command(commands=["id"], prefix="!")
)
def echo_id(client, message):
    message.reply_text(
        text=Config.responses.get("id").format(message.chat.id)
    )
