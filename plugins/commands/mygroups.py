# -*- coding: utf-8 -*-

# command: /mygroups
# region: private, OWNER
# description: groups which this bot is invited to by the owner

"""groups which this bot is invited to by the owner"""

__author__ = "Hadi Azami"

from pyrogram import Client, Filters
from pyrogram.errors import ChannelPrivate

from data import chat


@Client.on_message(
    Filters.command("mygroups", "/")
    & Filters.private
)
def mygroups(client, message):
    groups = chat.get_chats(message.chat.id)

    reply = ""

    for group in groups:
        try:
            gp = client.get_chat(
                chat_id=group.id
            )
            # Delete normal groups that are left alone
            if gp.members_count <= 1:
                chat.delete(gp.id)
                continue

            reply += (
                f"{gp.title}\n"
                f"id: {gp.id}\n"
            )
        except ChannelPrivate:
            # delete supergroups that are deleted
            chat.delete(group.id)

    if reply == "":
        message.reply_text(
            text="گروهی یافت نشد.",
            quote=False
        )
    else:
        message.reply_text(
            text=reply,
            quote=False
        )
