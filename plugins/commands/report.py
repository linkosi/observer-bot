# -*- coding: utf-8 -*-

# command: !report
# region: group
# description: report a message to admins

"""report a message to admins"""

__author__ = "Hadi Azami"

from pyrogram import Client, Filters

from helpers.filters import reply_to_bot
from helpers.functions import report, check_permissions


@Client.on_message(
    Filters.command(commands=["report"], prefix="!")
    & ~ Filters.create(reply_to_bot)
    & Filters.group
)
@check_permissions
def report_command(client, message):
    if message.reply_to_message is None:
        return

    # fix permission loophole
    message_copy = message
    message.delete()

    report(client, message_copy)
