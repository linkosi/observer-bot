# -*- coding: utf-8 -*-
__author__ = "abtinmo, mhazami"

""" detect badwords in message, report to admins """

from pyrogram import Client, Filters
from helpers.filters import bad_words
from helpers.functions import report


@Client.on_message(
    Filters.group
    & (Filters.text | Filters.caption)
    & Filters.create(bad_words)
)
def report_bad_words(client, message):
    report(client, message, is_bot=True)
