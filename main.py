# -*- coding: utf-8 -*-

"""Main entrypoint."""
__author__ = "Hadi Azami"

import logging

import pyrogram

from helpers.cfg import Config
import data.mongo_setup as mongo_setup

logging.basicConfig(
    level=logging.DEBUG,
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s'
)
logger = logging.getLogger(__name__)
logging.getLogger("pyrogram").setLevel(logging.WARNING)


def main():
    mongo_setup.init()

    app = pyrogram.Client(
        'observerbot',
        bot_token=Config.bot_token,
        plugins=dict(
            root="plugins"
        ),
        test_mode=False
    )

    app.run()


if __name__ == "__main__":
    main()
